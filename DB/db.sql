-- Database: BookAppointmentSystem

-- DROP DATABASE "BookAppointmentSystem";

CREATE DATABASE "BookAppointmentSystem"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

COMMENT ON DATABASE "BookAppointmentSystem"
    IS 'This is database for testApplication made at endava courses';